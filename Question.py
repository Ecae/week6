__author__ = 'jhoeflich2017'

class Question():
    def __init__(self, question, answer1, answer2, answer3, answer4, correct_answer):
        self.__question = question
        self.__answer1 = answer1
        self.__answer2 = answer2
        self.__answer3 = answer3
        self.__answer4 = answer4
        self.__correct_answer = correct_answer

    def ask(self):
        print(self.__question + '?:')
        print('1 ' + self.__answer1)
        print('2 ' + self.__answer2)
        print('3 ' + self.__answer3)
        print('4 ' + self.__answer4)


    def correct(self,response):
        if response == self.__correct_answer:
            return True
        else:
            return False

    def get_correct(self):
        print(self.__correct_answer)


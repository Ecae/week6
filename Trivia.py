
import Question
question = ['What the heck are those', 'It doesnt make a bit of difference guys?', 'What is the music of life?','What is the color of night?','What is lifes greatest illusion?','What do you put in a bucket to make it lighter?','How many buttons in a polo?','How many questions have you answered','Why are cats blind?','What is the best meme?']
answer1 = ['My ninja info cards','3', 'Drums?','Dark? Black I guess?','Innocence, my brother','A hole','1','6','Genetics :(','Meme']
answer2 = ['Mondo cool','The balls are inert','Triangle noise','Purple','Feminism','A torch','2','7','Russia','Memecubed']
answer3 = ['Crab apples','Rectangle','Silence, my brother','Open Salami','Cat-dad','My feelings','3','4','Bronies','Memesquared']
answer4 = ['Jumanji','Apple','Screaming','Sanguine, my brother','Ninja info cards','Pop punk','4','At least 3','Agent Orange','None']
correct = [1,2,3,4,1,2,3,4,1,2]
questionList = []
x = 0
turns = 10

while x < turns:
    q = Question.Question(question[x],answer1[x],answer2[x],answer3[x],answer4[x],correct[x])
    questionList.append(q)
    x += 1

p1 = 0
p2 = 0
turn = 0
while turn < turns:
    if turn%2 != 0:
        print('Player 2 answer')
        questionList[turn].ask()
        response = int(input('Enter a number '))
        if questionList[turn].correct(response) == True:
            p2 += 1
            print('Correct')
        else:
            print('Incorrect')
    if turn%2 == 0:
        print('Player 1 answer')
        questionList[turn].ask()
        response = int(input('Enter a number '))
        if questionList[turn].correct(response) == True:
            p1 += 1
            print('Correct')
        else:
            print('Incorrect')
    turn += 1